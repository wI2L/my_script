/*
** print_time.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/time
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:20:10 2012 william poussier
** Last update Thu Feb 16 14:20:14 2012 william poussier
*/

#include <stdio.h>
#include <sys/time.h>
#include "my_script.h"

void		print_time(s_timeval *tv, ssize_t c)
{
  t_options	*op;
  double	newtime;

  op = get_options();
  newtime = tv->tv_sec + (double)tv->tv_usec / 1000000;

  fprintf(op->timings, "%f %d\n", (newtime - op->oldtime), (int)c);
  op->oldtime = newtime;
}
