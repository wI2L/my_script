/*
** get_time.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/time
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:19:51 2012 william poussier
** Last update Thu Feb 16 14:23:01 2012 william poussier
*/

#include <time.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

char			*get_time(void)
{
  time_t	clock;
  static char	date[DATE_BUFF];
  const s_tm	*tm;

  if (xtime(&clock) != (time_t) -1)
    {
      tm = localtime(&clock);
      xstrftime(date, DATE_BUFF, "%c", tm);
      return (date);
    }
  return (DATE_ERROR_MSG);
}
