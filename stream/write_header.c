/*
** write_header.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/stream
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:19:30 2012 william poussier
** Last update Thu Feb 16 14:19:33 2012 william poussier
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "my_script.h"
#include "xlib.h"

int	write_header(t_options *op)
{
  char	*date;
  FILE	*stream;

  stream = get_stream();
  date = get_time();

  xfwrite("Script started on ", 18, sizeof(char), stream);
  xfwrite(date, strlen(date), sizeof(char), stream);
  xfwrite("\n", 1, sizeof(char), stream);

  if (op->q != 1)
    {
      xwrite(1, "Script started, file is ", 25);
      xwrite(1, op->filename, strlen(op->filename));
      xwrite(1, "\n", 1);
    }
  return (0);
}
