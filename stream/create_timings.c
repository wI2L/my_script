/*
** create_timings.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/stream
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:18:42 2012 william poussier
** Last update Thu Feb 16 14:18:45 2012 william poussier
*/

#include <stdio.h>
#include "my_script.h"

void		create_timings(char *file)
{
  t_options	*op;

  op = get_options();
  op->timings = xfopen(file, "w");

  fprintf(op->timings, "%f %d\n", 0.005000, 1);
}
