/*
** write_footer.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/stream
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:19:12 2012 william poussier
** Last update Thu Feb 16 14:19:17 2012 william poussier
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "my_script.h"
#include "xlib.h"

int	write_footer(t_options *op)
{
  char	*date;
  FILE	*stream;

  stream = get_stream();
  date = get_time();

  if (op->q != 1)
    {
      xwrite(1, "Script done, file is ", 21);
      xwrite(1, op->filename, strlen(op->filename));
      xwrite(1, "\n", 1);
      xfwrite("\nScript done on ", 16, sizeof(char), stream);
      xfwrite(date, strlen(date), sizeof(char), stream);
      xfwrite("\n", 1, sizeof(char), stream);
    }

  xfclose(stream);

  if (op->t == 1)
    xfclose(op->timings);

  return (0);
}
