/*
** get_stream.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/stream
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:18:57 2012 william poussier
** Last update Thu Feb 16 14:19:01 2012 william poussier
*/

#include <stdlib.h>
#include <stdio.h>
#include "my_script.h"
#include "xlib.h"

FILE		*get_stream(void)
{
  t_options	*op;
  static FILE	*stream = NULL;

  if (stream != NULL)
    return (stream);
  else
    {
      op = get_options();

      if (op->a == 1)
	stream = xfopen(op->filename, "a");
      else
	stream = xfopen(op->filename, "w");

      return (stream);
    }
  return (NULL);
}
