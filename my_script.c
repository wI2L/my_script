/*
** my_script.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:05:14 2012 william poussier
** Last update Thu Feb 16 14:05:22 2012 william poussier
*/

#include <pty.h>
#include <stdio.h>
#include <locale.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "my_script.h"
#include "xlib.h"

int		my_script(int argc, char *argv[])
{
  int		master;
  t_options	*op;

  op = get_options();
  op->child = my_forkpty(&master, NULL, NULL, NULL);

  if (op->child < 0)
    return (-1);
  else
    {
      parse_ext(argc, argv);
      if (op->child == 0)
	{
	  if (op->cmd == 0)
	    execl(op->shell, op->shname, "-i", NULL);
	  else
	    execl(op->shell, op->shname, "-c", op->cmd, NULL);
	  xwrite(2, "Shell not found.\nTerminated\n", 30);
	  exit(EXIT_FAILURE);
	}
      else if (op->child > 0)
	return (init(master, op->child));
    }
  return (EXIT_FAILURE);
}

int             main(int argc, char *argv[])
{
  t_options	*op;

  op = get_options();
  op->progname = get_progname(argv[0]);

  setlocale(LC_ALL, "");
  setlocale(LC_NUMERIC, "C");

  if (argc == 2)
    {
      if (strcmp(argv[1], "--help") == 0)
	usage();
    }
  return (my_script(argc, argv));
}
