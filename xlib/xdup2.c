/*
** xdup2.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:24:35 2012 william poussier
** Last update Thu Feb 16 14:24:38 2012 william poussier
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_script.h"
#include "xlib.h"

int		xdup2(int oldd, int newd)
{
  int		err;
  t_options	*op;

  if (dup2(oldd, newd) == -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: can't dup, error: %s\n",
	      op->progname, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (0);
}
