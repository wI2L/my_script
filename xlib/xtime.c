/*
** xtime.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:29:39 2012 william poussier
** Last update Thu Feb 16 14:29:42 2012 william poussier
*/

#include <string.h>
#include <stdio.h>
#include <time.h>
#include "my_script.h"
#include "xlib.h"

time_t		xtime(time_t *ltime)
{
  int		err;
  t_options	*op;

  if (time(ltime) == (time_t) -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: cannot retrieve the local time: %s\n",
	      op->progname, strerror(err));

      return ((time_t) -1);
    }
  return (*ltime);
}
