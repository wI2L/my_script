/*
** xselect.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:27:10 2012 william poussier
** Last update Thu Feb 16 14:27:25 2012 william poussier
*/

#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "my_script.h"
#include "xlib.h"

int             xselect(int nfds, fd_set *readfds, fd_set *writefds,
			fd_set *exceptfds, s_timeval *timeout)
{
  int           err;
  t_options     *op;
  int           ret;

  if ((ret = select(nfds, readfds, writefds,
		    exceptfds, timeout)) == -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: select failed, can't monitor terminals: %s\n",
	      op->progname, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (ret);
}
