/*
** xfopen.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:25:04 2012 william poussier
** Last update Thu Feb 16 14:25:07 2012 william poussier
*/

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

FILE		*xfopen(const char* path, const char *mode)
{
  int		err;
  t_options	*op;
  FILE		*stream;

  if ((stream = fopen(path, mode)) == NULL)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: fopen return an error, script aborted: %s\n",
	      op->progname, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (stream);
}
