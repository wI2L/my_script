/*
** xfwrite.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:25:52 2012 william poussier
** Last update Thu Feb 16 14:25:55 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include "my_script.h"
#include "xlib.h"

size_t		xfwrite(const void *ptr, size_t size,
			size_t nmemb, FILE *stream)
{
  int		err;
  t_options	*op;
  size_t	omemb;

  op = get_options();

  if ((omemb = fwrite(ptr, size, nmemb, stream)) < nmemb)
    {
      err = errno;
      fprintf(stderr,
	      "%s: cannot write script file, error: %s\n",
	      op->progname, strerror(err));
      error();
    }

  if (op->f == 1)
    fflush(stream);

  return (omemb);
}
