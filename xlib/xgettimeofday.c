/*
** xgettimeofday.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:26:10 2012 william poussier
** Last update Thu Feb 16 14:31:13 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/time.h>
#include "my_script.h"

int		xgettimeofday(s_timeval *tv, s_timezone *tz)
{
  int		err;
  t_options	*op;

  op = get_options();

  if (gettimeofday(tv, tz) == -1)
    {
      err = errno;
      fprintf(stderr,
	      "%s: local machine timings can't be retrieved: %s\n",
	      op->progname, strerror(err));
      error();
    }
  return (0);
}
