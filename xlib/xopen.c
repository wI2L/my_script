/*
** xopen.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:26:44 2012 william poussier
** Last update Thu Feb 16 14:26:47 2012 william poussier
*/

#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

int		xopen(const char* pathname, int flags)
{
  int		err;
  t_options	*op;
  int		fd;

  if ((fd = open(pathname, flags)) == -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: open return an error, script aborted: %s\n",
	      op->progname, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (fd);
}
