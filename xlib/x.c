/*
** x.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:24:02 2012 william poussier
** Last update Thu Feb 16 14:24:06 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "my_script.h"
#include "xlib.h"

void		x(char *strerr, char *file, int line, int failure)
{
  int		err;
  t_options	*op;

  err = errno;
  op = get_options();

  fprintf(stderr, "%s: %s:%d > %s: %s\n", op->progname,
	  file, line, strerr, strerror(err));

  if (failure == 1)
    exit(EXIT_FAILURE);
}
