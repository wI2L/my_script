/*
** xmalloc.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:26:30 2012 william poussier
** Last update Thu Feb 16 14:26:33 2012 william poussier
*/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "my_script.h"
#include "xlib.h"

void		*xmalloc(size_t size)
{
  int		err;
  t_options	*op;
  void		*none;

  if ((none = malloc(size)) == NULL)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: fatal error: malloc of size %lu failed: %s",
	      op->progname, size, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (none);
}
