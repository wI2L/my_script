/*
** xfclose.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:24:50 2012 william poussier
** Last update Thu Feb 16 14:24:53 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "my_script.h"
#include "xlib.h"

int		xfclose(FILE *fp)
{
  t_options	*op;
  int		err;

  if (fclose(fp) == EOF)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: fclose return an error: %s\n",
	      op->progname, strerror(err));

      exit(EXIT_FAILURE);
    }
  return (0);
}
