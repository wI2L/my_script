/*
** xwrite.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:29:55 2012 william poussier
** Last update Thu Feb 16 14:29:58 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "my_script.h"
#include "xlib.h"

ssize_t		xwrite(int fd, const void *buf, size_t count)
{
  int		err;
  t_options	*op;
  ssize_t	t;

  if ((t = write(fd, buf, count)) == -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: write error: %s\n",
	      op->progname, strerror(err));
      error();
    }
  return (t);
}
