/*
** xclose.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:24:21 2012 william poussier
** Last update Thu Feb 16 14:24:24 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "my_script.h"
#include "xlib.h"

int		xclose(int fd)
{
  t_options	*op;
  int		err;

  if (close(fd) == -1)
    {
      err = errno;
      op = get_options();

      fprintf(stderr,
	      "%s: close return an error: %s\n",
	      op->progname, strerror(err));

      return (-1);
    }
  return (0);
}
