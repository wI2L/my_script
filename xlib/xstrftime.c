/*
** xstrftime.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/xlib
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:27:39 2012 william poussier
** Last update Thu Feb 16 14:29:24 2012 william poussier
*/

#include <time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "my_script.h"

size_t		xstrftime(char *s, size_t max,
			  const char *format, const s_tm *tm)
{
  int           err;
  t_options     *op;
  size_t        res;

  op = get_options();

  if ((res = strftime(s, max, format, tm)) == 0)
    {
      err = errno;
      fprintf(stderr,
	      "%s: cannot retrieve the local formatted date: %s\n",
	      op->progname, strerror(err));

      strcpy(s, DATE_ERROR_MSG);
    }
  return (res);
}
