##
## Makefile for my_script in /u/all/poussi_w/rendu/c/my_script
##
## Made by william poussier
## Login   <poussi_w@epitech.net>
##
## Started on  Thu Feb 24 07:22:27 2011 william poussier
## Last update Thu Feb 16 14:03:42 2012 william poussier
##

NAME		=	my_script

SRC		=       ./my_script.c			\
			./login/my_forkpty.c		\
			./login/my_grantpt.c		\
			./login/my_login_tty.c		\
			./login/my_openpty.c		\
			./login/my_ptsname.c		\
			./login/my_unlockpt.c		\
			./login/my_posix_openpt.c	\
			./common/process.c		\
                        ./common/init.c			\
                        ./common/io_tty.c		\
			./common/signals.c		\
			./common/sigchld.c		\
			./common/capabilities.c		\
			./common/if_link.c		\
			./common/if_access.c		\
			./common/error.c		\
			./common/resize.c		\
			./parser/get_options.c		\
                        ./parser/get_shell.c		\
			./parser/get_progname.c		\
			./parser/is_option.c		\
                        ./parser/parse.c		\
                        ./parser/parse_options.c	\
			./parser/usage.c		\
			./stream/get_stream.c		\
                        ./stream/write_footer.c		\
                        ./stream/write_header.c		\
			./stream/create_timings.c	\
			./time/get_time.c		\
			./time/print_time.c

SRC_XLIB	=	./xlib/xselect.c		\
			./xlib/xgettimeofday.c		\
			./xlib/xstrftime.c		\
			./xlib/xclose.c			\
			./xlib/xfclose.c		\
			./xlib/xfwrite.c		\
                        ./xlib/xmalloc.c		\
                        ./xlib/xopen.c			\
			./xlib/xfopen.c			\
			./xlib/xwrite.c			\
			./xlib/xdup2.c			\
			./xlib/xtime.c			\
			./xlib/x.c


OBJ		= 	$(SRC:.c=.o)

OBJ_XLIB	=	$(SRC_XLIB:.c=.o)


CC              =       @gcc

STRIP		=	@strip

RM		= 	@rm -f

CFLAGS		= 	-Wall -W -Werror -Wno-unused-parameter -Wno-uninitialized -O3 -I./include

LFLAGS		=


all		:	$(NAME)

.c.o		:	$(CC) $(CFLAGS) $(INCLUDES) $(LFLAGS) -c $< -o $(<:.c=.o)

$(NAME)		:	$(OBJ) $(OBJ_XLIB)
			$(CC) -o $(NAME) $(OBJ) $(OBJ_XLIB) $(LFLAGS)
			$(STRIP) $(NAME)

clean		:
			@find . \( -name "*.o" -o -name "*~" -o -name "#*#" \) -exec rm {} \;
##			$(RM) $(OBJ)
##			$(RM) *~

fclean		:	clean
			$(RM) $(NAME)

re		: 	fclean all

.PHONY		:	all re clean fclean