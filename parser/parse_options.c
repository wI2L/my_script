/*
** parse_options.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:17:33 2012 william poussier
** Last update Thu Feb 16 14:17:40 2012 william poussier
*/

#include <unistd.h>
#include <getopt.h>
#include <string.h>
#include "my_script.h"

void		parse_options_ext(int argc, char *argv[])
{
  t_options	*op;
  int		opt;

  op = get_options();

  while ((opt = getopt(argc, argv, "ac:efhqt")) != -1)
    {
      if (opt == 'h')
	usage();
      if (opt == '?')
	usage();
      is_option_ext(opt, op);
    }
}
