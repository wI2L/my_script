/*
** get_progname.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:16:07 2012 william poussier
** Last update Thu Feb 16 14:16:11 2012 william poussier
*/

#include <stdlib.h>
#include <strings.h>
#include "my_script.h"

char	*get_progname(char *name)
{
  char	*foo;

  foo = rindex(name, '/');

  if (foo != NULL)
    return (foo + 1);

  return (name);
}
