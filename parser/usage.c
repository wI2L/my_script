/*
** usage.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:17:52 2012 william poussier
** Last update Thu Feb 16 14:17:57 2012 william poussier
*/

#include <stdio.h>
#include <stdlib.h>
#include "my_script.h"

void            usage(void)
{
  t_options     *op;

  op = get_options();

  fprintf(stderr,
	  "usage: %s [-a] [-c command] [-e] [-f] [-q] [-t] [file]\n"
	  "Use -h or --help to show this usage again.\n",
	  op->progname);

  exit(EXIT_SUCCESS);
}
