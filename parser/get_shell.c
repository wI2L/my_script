/*
** get_shell.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:16:26 2012 william poussier
** Last update Thu Feb 16 14:16:32 2012 william poussier
*/

#include <paths.h>
#include <stdlib.h>
#include <strings.h>
#include "my_script.h"

void		get_shell(void)
{
  t_options	*op;

  op = get_options();

  if ((op->shell = getenv("SHELL")) == NULL)
    op->shell = _PATH_BSHELL;

  op->shname = rindex(op->shell, '/');

  if (op->shname != NULL)
    op->shname += 1;
  else
    op->shname = op->shell;
}
