/*
** is_option.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:16:45 2012 william poussier
** Last update Thu Feb 16 14:16:58 2012 william poussier
*/

#include <unistd.h>
#include <string.h>
#include "my_script.h"

void	is_option_ext(int opt, t_options *op)
{
  if (opt == 'a')
      op->a = 1;
  if (opt == 'c')
    {
      op->c = 1;
      op->cmd = optarg;
    }
  if (opt == 'e')
    op->e = 1;
  if (opt == 'f')
    op->f = 1;
  if (opt == 'q')
    op->q = 1;
  if (opt == 't')
    {
      create_timings(TIMINGS_FILE);
      op->t = 1;
    }
}
