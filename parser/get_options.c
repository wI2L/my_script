/*
** get_options.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:15:52 2012 william poussier
** Last update Thu Feb 16 14:15:56 2012 william poussier
*/

#include <time.h>
#include <string.h>
#include <stdlib.h>
#include "my_script.h"

t_options		*get_options(void)
{
  static int		_is_set = 0;
  static t_options	op;

  if (_is_set == 0)
    {
      memset(&op, 0, sizeof(op));
      op.oldtime = time(NULL);
      _is_set = 1;
    }
  return (&op);
}
