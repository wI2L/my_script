/*
** parse.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/parser
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:17:16 2012 william poussier
** Last update Thu Feb 16 14:17:20 2012 william poussier
*/

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "my_script.h"

void		parse_ext(int argc, char *argv[])
{
  t_options	*op;

  op = get_options();
  parse_options_ext(argc, argv);

  if (optind < argc)
    op->filename = (argv[optind]);
  else
    op->filename = TYPESCRIPT;

  if_link(op->filename);
  if_access(op->filename);

  get_shell();
}
