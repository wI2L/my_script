/*
** sigchld.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:09:36 2012 william poussier
** Last update Thu Feb 16 14:09:42 2012 william poussier
*/

#include <sys/wait.h>
#include "my_script.h"

void		sigchld(int __attribute__((unused)) dummy)
{
  t_options	*op;
  int		status;
  int		pid;

  op = get_options();

  while ((pid = wait3(&status, WNOHANG, 0)) > 0)
    {
      if (pid == op->child)
	op->childstatus = status;
    }
}
