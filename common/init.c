/*
** init.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:08:20 2012 william poussier
** Last update Thu Feb 16 14:08:23 2012 william poussier
*/

#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

int		init(int master, pid_t pid)
{
  t_options	*op;

  op = get_options();

  write_header(op);
  signals();
  process(master);
  write_footer(op);

  if (op->e == 1)
    {
      if (WIFEXITED(op->childstatus))
	return (WEXITSTATUS(op->childstatus));
      if (WIFSIGNALED(op->childstatus))
	return (WTERMSIG(op->childstatus) + 0x80);
    }
  return (0);
}
