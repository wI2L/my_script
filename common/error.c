/*
** error.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:06:16 2012 william poussier
** Last update Thu Feb 16 14:06:19 2012 william poussier
*/

#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

void		error(void)
{
  char		*date;
  t_options	*op;
  FILE		*stream;

  stream = get_stream();
  date = get_time();
  op = get_options();
  kill(0, SIGTERM);

  xwrite(1, "Script aborted, file is ", 21);
  xwrite(1, op->filename, strlen(op->filename));
  xwrite(1, "\n", 1);
  xfwrite("\nScript aborted on ", 16, sizeof(char), stream);
  xfwrite(date, strlen(date), sizeof(char), stream);
  xfwrite("\n", 1, sizeof(char), stream);

  xfclose(stream);

  if (op->t == 1)
    xfclose(op->timings);

  exit(EXIT_FAILURE);
}
