/*
** capabilities.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:05:42 2012 william poussier
** Last update Thu Feb 16 14:06:01 2012 william poussier
*/

#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>
#include "my_script.h"

void		capabilities(void)
{
  s_termios	line;

  ioctl(0, TIOCGETA, &line);
  line.c_lflag &= ~(ICANON | ISIG | ECHO);
  ioctl(0, TIOCSETA, &line);
}
