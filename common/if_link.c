/*
** if_link.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:07:38 2012 william poussier
** Last update Thu Feb 16 14:08:06 2012 william poussier
*/

#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include "my_script.h"

void		if_link(char *filename)
{
  t_options	*op;
  s_stat	s;

  op = get_options();

  if (lstat(filename, &s) == 0
      && (S_ISLNK(s.st_mode)))
    {
      fprintf(stderr,
	      "Warning: '%s' is a link.\n"
	      "Script not started.\n",
	      filename);

      exit(EXIT_FAILURE);
    }
}
