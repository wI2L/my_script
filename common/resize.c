/*
** resize.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:09:08 2012 william poussier
** Last update Thu Feb 16 14:09:21 2012 william poussier
*/

#include <sys/ioctl.h>
#include "my_script.h"

void		resize(int master)
{
  s_winsize	winp;

  ioctl(1, TIOCGWINSZ, &winp);
  ioctl(master, TIOCSWINSZ, &winp);
}
