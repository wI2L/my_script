/*
** io_tty.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:08:36 2012 william poussier
** Last update Thu Feb 16 14:08:39 2012 william poussier
*/

#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include "my_script.h"
#include "xlib.h"

int		io_tty(char **buffer, ssize_t *readed, fd_set *readf, int master)
{
  t_options	*op;
  s_timeval	tv;

  if (FD_ISSET(0, readf))
    {
      if ((*readed = read(0, *buffer, sizeof(**buffer))) <= 0)
        return (-1);
      xwrite(master, *buffer, *readed);
    }

  op = get_options();
  xgettimeofday(&tv, NULL);

  if (FD_ISSET(master, readf))
    {
      if ((*readed = read(master, *buffer, sizeof(**buffer))) <= 0)
        return (-1);
      if (op->t == 1)
	print_time(&tv, *readed);
      xwrite(1, *buffer, *readed);
      return (1);
    }
  return (0);
}
