/*
** if_access.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:06:32 2012 william poussier
** Last update Thu Feb 16 14:07:24 2012 william poussier
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "my_script.h"

void	if_access(char *filename)
{
  int	err;

  if (access(filename, F_OK) == 0)
    {
      if (access(filename, W_OK) == -1)
	{
	  err = errno;
	  fprintf(stderr,
		  "%s: %s\n"
		  "Terminated\n",
		  filename, strerror(err));

	  exit(EXIT_FAILURE);
	}
    }
}
