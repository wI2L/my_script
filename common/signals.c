/*
** signals.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:09:55 2012 william poussier
** Last update Thu Feb 16 14:09:58 2012 william poussier
*/

#include <wait.h>
#include <signal.h>
#include <termios.h>
#include "my_script.h"
#include "xlib.h"

void		signals(void)
{
  s_sigaction	sa;

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = sigchld;
  sigaction(SIGCHLD, &sa, NULL);
}
