/*
** process.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/common
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:08:52 2012 william poussier
** Last update Thu Feb 16 14:08:55 2012 william poussier
*/

#include <pty.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include "my_script.h"
#include "xlib.h"

void			process(int master)
{
  int			ret;
  ssize_t		readed;
  fd_set		readf;
  char			*buffer;

  readed = 0;
  capabilities();
  buffer = xmalloc(BUFF_SIZE * sizeof(char));

  while (42 && (ret != -1))
    {
      resize(master);
      FD_ZERO(&readf);
      FD_SET(0, &readf);
      FD_SET(master, &readf);

      if (xselect(master + 1, &readf, NULL, NULL, NULL) >= 0)
        {
          if ((ret = io_tty(&buffer, &readed, &readf, master)) == 1 )
	    xfwrite(buffer, readed, sizeof(char), get_stream());
	}
    }
  free(buffer);
}
