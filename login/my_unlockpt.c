/*
** my_unlockpt.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:14:57 2012 william poussier
** Last update Thu Feb 16 14:15:00 2012 william poussier
*/

#include <errno.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include "my_script.h"

int	my_unlockpt(int fd)
{
#ifdef TIOCSPTLCK
  int	errno_b;
  int	foo;

  foo = 0;
  errno_b = errno;
  if (ioctl(fd, TIOCSPTLCK, &foo))
    {
      if (errno == EINVAL)
	{
	  errno = errno_b;
	  return (0);
	}
      else
	return (-1);
    }
#endif
  return (0);
}
