/*
** my_openpty.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:14:06 2012 william poussier
** Last update Thu Feb 16 14:14:10 2012 william poussier
*/

#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include "my_script.h"
#include "xlib.h"

int     my_openpty(int *amaster, int *aslave, char *name,
                  s_termios *termp, s_winsize *winp)
{
  char  *slaven;

  if ((*amaster = my_posix_openpt(O_RDWR)) == -1)
    X("my_posix_openpt", 1, 0);

  if ((slaven = my_ptsname(*amaster)) == NULL)
    X("my_ptsname", 1, 0);

  my_grantpt(*amaster);
  my_unlockpt(*amaster);
  *aslave = xopen(slaven, O_RDWR);

  if (name != NULL)
    name = strcpy(name, slaven);

  return (0);
}
