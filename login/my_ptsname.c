/*
** my_ptsname.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:14:42 2012 william poussier
** Last update Thu Feb 16 14:14:45 2012 william poussier
*/

#include <stdio.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "my_script.h"

char		*my_ptsname(int fd)
{
  unsigned int	pty_n;
  static char	pts_name[64];

  pty_n = 0;

  if (ioctl(fd, TIOCGPTN, &pty_n) != 0)
    X("ioctl", 1, 0);

  snprintf(pts_name, sizeof(pts_name), "/dev/pts/%u", pty_n);

  return (pts_name);
}
