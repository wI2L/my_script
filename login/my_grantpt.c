/*
** my_grantpt.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:13:35 2012 william poussier
** Last update Thu Feb 16 14:13:39 2012 william poussier
*/

#include <termios.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include "my_script.h"

int		my_grantpt(int fd)
{
  struct stat	st;

  if (fstat(fd, &st) < 0)
    {
      X("fstat", 0, 1);
      return (-1);
    }
  if (chmod((char*)my_ptsname(fd),
	    st.st_mode | S_IRUSR | S_IWUSR | S_IWGRP) < 0)
    {
      X("chmod", 0, 2);
      return (-1);
    }
  return (0);
}
