/*
** my_forkpty.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:12:48 2012 william poussier
** Last update Thu Feb 16 14:13:25 2012 william poussier
*/

#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <utmp.h>
#include <pty.h>
#include "my_script.h"
#include "xlib.h"

int     my_forkpty(int *amaster, char *name,
		   s_termios *termp, s_winsize *winp)
{
  int   master;
  int	slave;
  pid_t pid;

  my_openpty(&master, &slave, name, termp, winp);

  if ((pid = fork()) == -1)
    {
      xclose(master);
      xclose(slave);
      return (-1);
    }
  if (pid == 0)
    {
      xclose(master);
      if (my_login_tty(slave) == -1)
        return (-1);
      return (0);
    }
  else
    {
      *amaster = master;
      xclose(slave);
      return (pid);
    }
}
