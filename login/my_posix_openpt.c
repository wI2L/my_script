/*
** my_posix_openpt.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:14:25 2012 william poussier
** Last update Thu Feb 16 14:14:29 2012 william poussier
*/

#include <stdio.h>
#include "my_script.h"
#include "xlib.h"

int	my_posix_openpt(int flags)
{
  int	fd;

  fd = xopen("/dev/ptmx", flags);

  if (fd == -1)
    X("xopen", 1, 0);

  return (fd);
}
