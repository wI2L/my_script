/*
** my_login_tty.c for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/login
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:13:49 2012 william poussier
** Last update Thu Feb 16 14:13:53 2012 william poussier
*/

#include <sys/types.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include "my_script.h"
#include "xlib.h"

int	my_login_tty(int fd)
{

  if (setsid() == -1)
    X("setsid", 1, 0);
  if (ioctl(fd, TIOCSCTTY, (char *) 1) == -1)
    X("ioctl", 1, 0);

  xdup2(fd, 0);
  xdup2(fd, 1);
  xdup2(fd, 2);

  if (fd > 2)
    xclose(fd);

  return (0);
}
