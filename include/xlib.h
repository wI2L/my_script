/*
** xlib.h for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:11:45 2012 william poussier
** Last update Thu Feb 16 14:11:58 2012 william poussier
*/

#ifndef	__XLIB_H__
#define __XLIB_H__

# include <sys/types.h>
# include <unistd.h>
# include <stdio.h>
# include <errno.h>
# include <time.h>

# define X(str, fail, off)   (x(str, __FILE__, (__LINE__ - off - 1), fail))

int	xclose(int);
int	xfclose(FILE *);
int     xdup2(int, int);
int	xopen(const char *, int);
int     xgettimeofday(struct timeval *, struct timezone *);
int	xselect(int, fd_set *, fd_set *, fd_set *, struct timeval *);

FILE	*xfopen(const char *, const char *);

time_t  xtime(time_t *);

size_t	xfwrite(const void *, size_t, size_t, FILE *);
size_t	xstrftime(char *, size_t, const char *, const struct tm *);

ssize_t	xwrite(int, const void *, size_t);

void    *xmalloc(size_t);
void	x(char *, char *, int, int);

#endif	/* !__XLIB_H_ */
