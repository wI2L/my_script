/*
** my_script.h for my_script in /home/poussi_w//work/myscript-2015-2014s-poussi_w/include
**
** Made by william poussier
** Login   <poussi_w@epitech.net>
**
** Started on  Thu Feb 16 14:12:19 2012 william poussier
** Last update Thu Feb 16 14:32:14 2012 william poussier
*/

#ifndef		__MY_SCRIPT_H__
# define	__MY_SCRIPT_H__

#ifndef TIOCGETA
# define TIOCGETA TCGETS
# define TIOCSETA TCSETS
#endif

# include <errno.h>
# include <stdio.h>
# include <unistd.h>
# include <termios.h>
# include <sys/time.h>
# include <sys/ioctl.h>
# include <sys/types.h>
# include "xlib.h"

# define	DATE_ERROR_MSG		"no-time-retrieved"
# define	TIMINGS_FILE		"timings"
# define	TYPESCRIPT		"typescript"

# define	BUFF_SIZE		1024
# define	DATE_BUFF		40

typedef	struct	winsize			s_winsize;
typedef	struct	termios			s_termios;
typedef	struct	timeval			s_timeval;
typedef struct	sigaction		s_sigaction;
typedef struct	stat			s_stat;
typedef struct	tm			s_tm;
typedef struct	timezone		s_timezone;

typedef	struct	s_options
{
  int		a;
  int		c;
  int		e;
  int		f;
  int		q;
  int		t;
  int		childstatus;
  pid_t		child;
  double	oldtime;
  char		*filename;
  char		*cmd;
  char		*shell;
  char		*shname;
  char		*progname;
  FILE		*timings;
}		t_options;

int		init(int, pid_t);
int		my_script(int, char **);
int		write_footer(t_options *);
int		write_header(t_options *);
int		io_tty(char **, ssize_t *, fd_set *, int);

int		my_grantpt(int);
int		my_unlockpt(int);
int		my_login_tty(int);
int		my_posix_openpt(int);
int		my_forkpty(int *, char *, s_termios *, s_winsize *);
int		my_openpty(int *, int *, char *, s_termios *, s_winsize *);

char		*get_time(void);
char		*my_ptsname(int);
char		*get_progname(char *);

t_options	*get_options(void);

void		usage(void);
void		error(void);
void		resize(int);
void		process(int);
void		signals(void);
void		get_shell(void);
void		if_link(char *);
void		if_access(char *);
void		capabilities(void);
void            create_timings(char *);
void		parse_ext(int, char **);
void		is_option_ext(int, t_options *);
void		parse_options_ext(int, char **);
void		sigchld(int __attribute__((unused)));
void		print_time(s_timeval *, ssize_t);

FILE		*get_stream(void);

#endif	/* !__MY_SCRIPT_H_ */
